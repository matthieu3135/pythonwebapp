# PythonWebApp

The aim of this python application will be to display a webpage on http://localhost:5000.

## How to install the app

First, you will have to clone this repository. Then, configure git so that, you can modify and play with the app and push it to your own repository. 
```
git clone https://gitlab.com/matthieu3135/pythonwebapp.git

git config --global user.name "your username"
git config --global user.email "your@email_address"

```
You will have to clone your own gitlab project in another location (the second line will be "mv pythonwebapp your_repository"). This gitlab project that you create must be empty, without a readme
``` 
git clone https://gitlab.com/username/repository

mv path_to_pythonwebapp path_to_repository

git init --initial-branch=main
git remote add origin https://gitlab.com/username/repository
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```
After this last command, your credentials will be needed, I recommend to use a token instead of the password
These previous commands are mandatory only for the first connection, then, only the last 3 of them will be needed.

## How to use the app

In order to use the app, we recommand to be in the same directory than the file app.py
You will have firstly to install the prerequisites, and then, you will be able to run the python script. The app will be visible in your browser at the address 127.0.0.1:5000

```
cd app
pip3 install -r requirements.txt
python3 app.py
```

# Have fun!
